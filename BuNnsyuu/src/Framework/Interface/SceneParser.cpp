#include "SceneParser.h"
using namespace BuNnsyuu;
using namespace std;

const shared_ptr<SceneObjectCamera> Scene::GetCamera(std::string key) const
{
	auto i = Cameras.find(key);
	return (i == Cameras.end() ? nullptr : i->second);
}

const std::shared_ptr<SceneCameraNode> BuNnsyuu::Scene::GetFirstCameraNode() const
{
	return (CameraNodes.empty() ? nullptr : CameraNodes.cbegin()->second);
}

const std::shared_ptr<SceneCameraNode> BuNnsyuu::Scene::GetNextCameraNode() const
{
	static thread_local auto _it = CameraNodes.cbegin();
	if (_it == CameraNodes.cend()) return nullptr;
	return ((++_it == CameraNodes.cend()) ? nullptr : _it->second);
}

const shared_ptr<SceneObjectLight> Scene::GetLight(std::string key) const
{
	auto i = Lights.find(key);
	return (i == Lights.end() ? nullptr : i->second);
}

const std::shared_ptr<SceneLightNode> BuNnsyuu::Scene::GetFirstLightNode() const
{
	return (LightNodes.empty() ? nullptr : LightNodes.cbegin()->second);
}

const std::shared_ptr<SceneLightNode> BuNnsyuu::Scene::GetNextLightNode() const
{
	static thread_local auto _it = LightNodes.cbegin();
	if (_it == LightNodes.cend()) return nullptr;
	return ((++_it == LightNodes.cend()) ? nullptr : _it->second);
}

const shared_ptr<SceneObjectGeometry> Scene::GetGeometry(string key) const
{
	auto i = Geometries.find(key);
	return (i == Geometries.end() ? nullptr : i->second);
}

const std::shared_ptr<SceneGeometryNode> BuNnsyuu::Scene::GetFirstGeometryNode() const
{
	return (GeometryNodes.empty() ? nullptr : GeometryNodes.cbegin()->second);
}

const std::shared_ptr<SceneGeometryNode> BuNnsyuu::Scene::GetNextGeometryNode() const
{
	static thread_local auto _it = GeometryNodes.cbegin();
	if (_it == GeometryNodes.cend()) return nullptr;
	return ((++_it == GeometryNodes.cend()) ? nullptr : _it->second);
}

const shared_ptr<SceneObjectMaterial> Scene::GetMaterial(string key) const
{
	auto i = Materials.find(key);
	return (i == Materials.end() ? nullptr : i->second);
}

const std::shared_ptr<SceneObjectMaterial> BuNnsyuu::Scene::GetFirstMaterial() const
{
	return (Materials.empty() ? nullptr : Materials.cbegin()->second);
}

const std::shared_ptr<SceneObjectMaterial> BuNnsyuu::Scene::GetNextMaterial() const
{
	static thread_local auto _it = Materials.cbegin();
	if (_it == Materials.cend()) return nullptr;
	return ((++_it == Materials.cend()) ? nullptr : _it->second);
}


