#pragma once
#include "PIMath.h"
#include "IRuntimeModule.h"
#include "SceneParser.h"

namespace BuNnsyuu
{
	class SceneManager : implements IRuntimeModule
	{
	public:
		virtual ~SceneManager();

		virtual int Initialize();
		virtual void Finalize();

		virtual void Tick();

		void LoadScene(const char* scene_file_name);

		const Scene& GetSceneForRendering();
	protected:
		void LoadOgexScene(const char* ogex_scene_file_name);
	protected:
		std::unique_ptr<Scene> m_pScene;
	};

	extern SceneManager* g_pSceneManager;
}