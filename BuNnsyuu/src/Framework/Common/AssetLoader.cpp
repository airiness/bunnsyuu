#include "AssetLoader.h"

int BuNnsyuu::AssetLoader::Initialize()
{
	return 0;
}

void BuNnsyuu::AssetLoader::Finalize()
{
	m_strSearchPath.clear();
}

void BuNnsyuu::AssetLoader::Tick()
{

}

bool BuNnsyuu::AssetLoader::AddSearchPath(const char * path)
{
	auto src = m_strSearchPath.begin();

	while (src != m_strSearchPath.end())
	{
		if (!(*src).compare(path))
		{
			return true;
		}
		src++;
	}

	m_strSearchPath.push_back(path);
	return true;
}

bool BuNnsyuu::AssetLoader::RemoveSearchPath(const char * path)
{
	auto src = m_strSearchPath.begin();
	while (src != m_strSearchPath.end())
	{
		if (!(*src).compare(path))
		{
			m_strSearchPath.erase(src);
			return true;
		}
		src++;
	}
	return false;
}

bool BuNnsyuu::AssetLoader::FileExists(const char * filePath)
{
	AssetFilePtr fp = OpenFile(filePath, BUNNSYUU_OPEN_BINARY);
	if (fp != nullptr)
	{
		CloseFile(fp);
		return true;
	}

	return false;
}

BuNnsyuu::AssetLoader::AssetFilePtr BuNnsyuu::AssetLoader::OpenFile(const char * name, AssetOpenMode mode)
{
	FILE *fp = nullptr;

	std::string upPath;
	std::string fullPath;
	for (size_t i = 0; i < 10; i++)
	{
		auto src = m_strSearchPath.begin();
		bool looping = true;
		while (looping)
		{
			fullPath.assign(upPath);
			if (src != m_strSearchPath.end())
			{
				fullPath.append(*src);
				fullPath.append("/assets/");
				src++;
			}
			else
			{
				fullPath.append("assets/");
				looping = false;
			}
			fullPath.append(name);
#ifdef _DEBUG
			fprintf(stderr, "Trying to open %s\n", fullPath.c_str());
#endif // _DEBUG
			switch (mode)
			{
			case BuNnsyuu::AssetLoader::BUNNSYUU_OPEN_TEXT:
				fp = fopen(fullPath.c_str(), "r");
				break;
			case BuNnsyuu::AssetLoader::BUNNSYUU_OPEN_BINARY:
				fp = fopen(fullPath.c_str(), "rb");
				break;
			default:
				break;
			}

			if (fp)
			{
				return (AssetFilePtr)fp;
			}
		}
		upPath.append("../");
	}


	return nullptr;
}

BuNnsyuu::Buffer BuNnsyuu::AssetLoader::SyncOpenAndReadText(const char * filePath)
{
	AssetFilePtr fp = OpenFile(filePath, BUNNSYUU_OPEN_TEXT);
	Buffer* pBuffer = nullptr;

	if (fp)
	{
		size_t length = GetSize(fp);
		pBuffer = new Buffer(length + 1);
		length = fread(pBuffer->m_pData, 1, length, static_cast<FILE*>(fp));
		pBuffer->m_pData[length] = '\0';

		CloseFile(fp);

#ifdef _DEBUG
		fprintf(stderr, "Read file '%s',%d bytes\n", filePath, length);
#endif // _DEBUG
	}
	else
	{
		fprintf(stderr, "Error opening file '%s'\n", filePath);
		pBuffer = new Buffer();
	}
	return *pBuffer;
}

BuNnsyuu::Buffer BuNnsyuu::AssetLoader::SyncOpenAndReadBinary(const char * filePath)
{
	AssetFilePtr fp = OpenFile(filePath, BUNNSYUU_OPEN_BINARY);
	Buffer* pBuffer = nullptr;

	if (fp)
	{
		size_t length = GetSize(fp);

		pBuffer = new Buffer(length);
		fread(pBuffer->m_pData, length, 1, static_cast<FILE*>(fp));

		CloseFile(fp);
#ifdef _DEBUG
		fprintf(stderr, "Read file '%s','%d bytes\n", filePath, length);
#endif // _DEBUG
	}
	else
	{
		fprintf(stderr, "Error opening file '%s'\n", filePath);
		pBuffer = new Buffer();
	}

	return *pBuffer;
}

size_t BuNnsyuu::AssetLoader::SyncRead(const AssetFilePtr & fp, Buffer & buf)
{
	size_t sz;
	if (!fp)
	{
		fprintf(stderr, "null file discriptor\n");
		return 0;
	}

	sz = fread(buf.m_pData, buf.m_szSize, 1, static_cast<FILE*>(fp));

	return sz;

	return size_t();
}

void BuNnsyuu::AssetLoader::CloseFile(AssetFilePtr & fp)
{
	fclose((FILE*)fp);
	fp = nullptr;
}

size_t BuNnsyuu::AssetLoader::GetSize(const AssetFilePtr & fp)
{
	FILE* _fp = static_cast<FILE*>(fp);

	//find file position indicator,get file length set indicator back
	long pos = ftell(_fp);
	fseek(_fp, 0, SEEK_END);
	size_t length = ftell(_fp);
	fseek(_fp, pos, SEEK_SET);

	return length;
}

int32_t BuNnsyuu::AssetLoader::Seek(AssetFilePtr fp, long offset, AssetSeekBase where)
{
	return fseek(static_cast<FILE*>(fp), offset, static_cast<int>(where));
}
