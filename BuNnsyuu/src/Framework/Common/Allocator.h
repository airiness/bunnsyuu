//size editable , not thread safe
#pragma once
#include <cstdint>

namespace BuNnsyuu
{
	struct BlockHeader
	{
		BlockHeader* pNext;
	};

	struct PageHeader
	{
		PageHeader* pNext;

		//helpr function that gives the first block
		BlockHeader* Blocks(void)
		{
			//note this+1,'this' is saving pNext
			return reinterpret_cast<BlockHeader*>(this + 1);
		}
	};

	class Allocator
	{
	public:
		//debug patterns
		static const uint8_t PATTERN_ALIGN = 0XFC;
		static const uint8_t PATTERN_ALLOC = 0XFD;
		static const uint8_t PATTERN_FREE = 0XFE;

		Allocator();
		Allocator(size_t data_size, size_t page_size, size_t alignment);
		~Allocator();

		//reset the allocator to a new configuration
		void Reset(size_t data_size, size_t page_size, size_t alignment);

		//alloc and free blocks
		void* Allocate();
		void Free(void* p);
		void FreeAll();
	private:
#if defined(_DEBUG)

		//fill a free page with debug patterns
		void FillFreePage(PageHeader* pPage);

		//fill a block with debug patterns
		void FillFreeBlock(BlockHeader* pBlock);

		//fill an allocated block with debug patterns
		void FillAllocatedBlock(BlockHeader* pBlock);
#endif // defined(_DEBUG)

		//get next block
		BlockHeader* NextBlock(BlockHeader* pBlock);

		//the page list
		PageHeader* m_pPageList;

		//the free block list
		BlockHeader* m_pFreeList;

		size_t m_szDataSize;
		size_t m_szPageSize;
		size_t m_szAlignmentSize;	//block size - minimal size
		size_t m_szBlockSize;
		uint32_t m_nBlocksPerPage;

		//statistics
		uint32_t m_nPages;
		uint32_t m_nBlocks;
		uint32_t m_nFreeBlocks;

		Allocator(const Allocator&) = delete;
		Allocator &operator=(const Allocator&) = delete;
	};
}