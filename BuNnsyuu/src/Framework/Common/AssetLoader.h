#pragma once
#include <string>
#include <vector>
#include "IRuntimeModule.h"
#include "Buffer.h"

namespace BuNnsyuu
{
	class AssetLoader : public IRuntimeModule
	{
	public:
		virtual ~AssetLoader() {};

		virtual int Initialize();
		virtual void Finalize();

		virtual void Tick();

		using AssetFilePtr = void*;

		enum AssetOpenMode
		{
			BUNNSYUU_OPEN_TEXT = 0,
			BUNNSYUU_OPEN_BINARY = 1,
		};

		enum AssetSeekBase
		{
			BUNNSYUU_SEEK_SET = 0,
			BUNNSYUU_SEEK_CUR = 1,
			BUNNSYUU_SEEK_END = 2,
		};

		bool AddSearchPath(const char* path);
		bool RemoveSearchPath(const char* path);
		bool FileExists(const char* filePath);

		AssetFilePtr OpenFile(const char* name, AssetOpenMode mode);
		Buffer SyncOpenAndReadText(const char* filePath);
		Buffer SyncOpenAndReadBinary(const char* filePath);
		size_t SyncRead(const AssetFilePtr& fp, Buffer& buf);
		void CloseFile(AssetFilePtr& fp);
		size_t GetSize(const AssetFilePtr& fp);
		int32_t Seek(AssetFilePtr fp, long offset, AssetSeekBase where);

		inline std::string SyncOpenAndReadTextFileToString(const char* fileName)
		{
			std::string result;
			Buffer buffer = SyncOpenAndReadText(fileName);
			char* content = reinterpret_cast<char*>(buffer.m_pData);

			if (content)
			{
				result = std::string(std::move(content));
			}

			return result;

		}
	private:
		std::vector<std::string> m_strSearchPath;

	};

	extern AssetLoader* g_pAssetLoader;
}