#pragma once
#include <cstdint>
#include <iostream>

namespace BuNnsyuu
{
	struct GfxConfiguration
	{
		/// Inline all-elements constructor.
        /// \param[in] r the red color depth in bits
        /// \param[in] g the green color depth in bits
        /// \param[in] b the blue color depth in bits
        /// \param[in] a the alpha color depth in bits
        /// \param[in] d the depth buffer depth in bits
        /// \param[in] s the stencil buffer depth in bits
        /// \param[in] msaa the msaa sample count
        /// \param[in] width the screen width in pixel
        /// \param[in] height the screen height in pixel
		GfxConfiguration(
			uint32_t r = 8,
			uint32_t g = 8,
			uint32_t b = 8,
			uint32_t a = 8,
			uint32_t d = 24,
			uint32_t s = 0,
			uint32_t msaa = 0,
			uint32_t width = 1920,
			uint32_t height = 1080,
			const wchar_t * name = L"BuNnsyuu")
			:redBits(r),
			greenBits(g),
			blueBits(b),
			alphaBits(a),
			depthBits(d),
			stencilBits(s),
			msaaSamples(msaa),
			screenWidth(width),
			screenHeight(height),
			appName(name)
		{}

		uint32_t redBits;
		uint32_t greenBits;
		uint32_t blueBits;
		uint32_t alphaBits;
		uint32_t depthBits;
		uint32_t stencilBits;
		uint32_t msaaSamples;
		uint32_t screenWidth;
		uint32_t screenHeight;
		const wchar_t* appName;

		friend std::wostream& operator<<(std::wostream& out, const GfxConfiguration& conf)
		{
			out << "GfxConfiguration" <<
				" R:" << conf.redBits <<
				" G:" << conf.greenBits <<
				" B:" << conf.blueBits <<
				" A:" << conf.alphaBits <<
				" D:" << conf.depthBits <<
				" S:" << conf.stencilBits <<
				" M:" << conf.msaaSamples <<
				" W:" << conf.screenWidth <<
				" H:" << conf.screenHeight <<
				std::endl;
			return out;
		}
	};
}