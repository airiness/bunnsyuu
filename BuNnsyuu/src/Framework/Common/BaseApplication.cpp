#include "BaseApplication.h"
#include <iostream>

bool BuNnsyuu::BaseApplication::m_bQuit = false;

BuNnsyuu::BaseApplication::BaseApplication(GfxConfiguration & cfg)
	:m_Config(cfg)
{

}

int BuNnsyuu::BaseApplication::Initialize()
{
	int result = 0;
	std::wcout << m_Config;
	return result;
}

//finalize all sub modules and clean up all runtime temporary files
void BuNnsyuu::BaseApplication::Finalize()
{
}

//one main loop cycle
void BuNnsyuu::BaseApplication::Tick()
{
}

bool BuNnsyuu::BaseApplication::IsQuit()
{
	return m_bQuit;
}
