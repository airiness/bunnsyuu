#include "SceneManger.h"
#include "AssetLoader.h"
#include "OGEX.h"
using namespace BuNnsyuu;

SceneManager::~SceneManager()
{
}

int SceneManager::Initialize()
{
	int result = 0;
	return result;
}

void SceneManager::Finalize()
{
}

void SceneManager::Tick()
{
}

void SceneManager::LoadScene(const char * scene_file_name)
{
	LoadOgexScene(scene_file_name);
}

const Scene & SceneManager::GetSceneForRendering()
{
	return *m_pScene;
}

void SceneManager::LoadOgexScene(const char * ogex_scene_file_name)
{
	std::string ogex_text = g_pAssetLoader->SyncOpenAndReadTextFileToString(ogex_scene_file_name);

	OgexParser ogex_parser;
	m_pScene = ogex_parser.Parse(ogex_text);
}
