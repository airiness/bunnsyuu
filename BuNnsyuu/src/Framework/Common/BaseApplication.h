#pragma once
#include "IApplication.h"
#include "GfxConfiguration.h"
namespace BuNnsyuu
{
	class BaseApplication : implements IApplication
	{
	public:
		BaseApplication(GfxConfiguration& cfg);

		virtual int Initialize();
		virtual void Finalize();
		virtual void Tick();
		virtual bool IsQuit();

		inline GfxConfiguration& GetConfiguration() { return m_Config; };

	protected:
		virtual void OnDraw() {};
	protected:
		//quit flag
		static bool m_bQuit;
		GfxConfiguration m_Config;

	private:
		//hide default construct enforce configuration
		BaseApplication() {};
	};

}


