#include <iostream> 
#include <tchar.h>
#include "IApplication.h"
#include "GraphicsManager.h"
#include "MemoryManager.h"
#include "AssetLoader.h"
#include "SceneManger.h"

using namespace BuNnsyuu;

int main(int argc, char** argv)
{
	int ret;

	if ((ret = g_pApp->Initialize()) != 0)
	{
		std::wcout << _T("App Initialize failed,will exit now.") << std::endl;
		return ret;
	}

	if ((ret = g_pMemoryManager->Initialize()) != 0)
	{
		std::wcout << _T("memorymanager initialize failed,exit now") << std::endl;
		return ret;
	}

	if ((ret = g_pGraphicsManager->Initialize()) != 0)
	{
		std::wcout << _T("graphics Initialize failed,will exit now.") << std::endl;
		return ret;
	}

	if ((ret = g_pAssetLoader->Initialize()) != 0)
	{
		std::wcout << _T("AssetLoader Initialize failed, exit now.") << std::endl;
		return ret;
	}

	if ((ret = g_pSceneManager->Initialize()) != 0)
	{
		std::wcout << _T("SceneManager Initialize failed, exit now.") << std::endl;
		return ret;
	}

	g_pSceneManager->LoadScene("Scene/cube.ogex");

	while (!g_pApp->IsQuit())
	{
		g_pApp->Tick();
		g_pMemoryManager->Tick();
		g_pGraphicsManager->Tick();
		g_pAssetLoader->Tick();
		g_pSceneManager->Tick();
		
	}

	g_pSceneManager->Finalize();
	g_pAssetLoader->Finalize();
	g_pGraphicsManager->Finalize();
	g_pMemoryManager->Finalize();
	g_pApp->Finalize();

	return 0;
}