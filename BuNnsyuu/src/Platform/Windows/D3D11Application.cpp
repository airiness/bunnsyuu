#include <tchar.h>
#include "WindowsApplication.h"
#include "DirectX3D/D3D11GraphicsManager.h"
#include "MemoryManager.h"
#include "AssetLoader.h"
#include "SceneManger.h"

namespace BuNnsyuu
{
	GfxConfiguration config(8, 8, 8, 8, 32, 0, 0, 1280, 720, _T("BuNnsyuu(Windows-DirectX11)"));
	IApplication* g_pApp = static_cast<IApplication*>(new WindowsApplication(config));
	GraphicsManager* g_pGraphicsManager = static_cast<GraphicsManager*>(new D3D11GraphicsManager);
	MemoryManager* g_pMemoryManager = static_cast<MemoryManager*>(new MemoryManager);
	AssetLoader* g_pAssetLoader = static_cast<AssetLoader*>(new AssetLoader);
	SceneManager* g_pSceneManager = static_cast<SceneManager*>(new SceneManager);
}