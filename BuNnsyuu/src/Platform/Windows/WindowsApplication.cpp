#include "WindowsApplication.h"
#include <tchar.h>
using namespace BuNnsyuu;

int WindowsApplication::Initialize()
{
	int result;
	result = BaseApplication::Initialize();

	if (result != 0)
	{
		exit(result);
	}

	//get the console instance
	HINSTANCE hInstance = GetModuleHandle(nullptr);

	WNDCLASSEXW wcex = {0};
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WindowProc;
	wcex.hInstance = hInstance;
	wcex.hCursor = LoadCursorW(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszClassName = _T("BuNnsyuu");

	if (!RegisterClassExW(&wcex))
	{
		return 1;
	}

	RECT rc = { 0,0,static_cast<LONG>(m_Config.screenWidth),static_cast<LONG>(m_Config.screenHeight) };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

	HWND hWnd = CreateWindowExW(0,
		_T("BuNnsyuu"),
		m_Config.appName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		rc.right - rc.left,
		rc.bottom - rc.top,
		nullptr,
		nullptr,
		hInstance,
		nullptr);

	ShowWindow(hWnd, SW_SHOWNORMAL);

	m_hWnd = hWnd;
	return result;
}

void WindowsApplication::Finalize()
{
}

void WindowsApplication::Tick()
{
	MSG msg;
	if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

LRESULT CALLBACK WindowsApplication::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_PAINT:
		break;
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		BaseApplication::m_bQuit = true;
		return 0;
	}
	default:
		break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}
