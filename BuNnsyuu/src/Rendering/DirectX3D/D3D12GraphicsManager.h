#pragma once
#include <cstdint>
#include <d3d12.h>
#include <d3dcompiler.h>
#include <dxgi1_4.h>
#include "GraphicsManager.h"
#include "Buffer.h"
#include "Image.h"

#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

namespace BuNnsyuu
{
	class D3D12GraphicsManager : public GraphicsManager
	{
	public:
		virtual int Initialize();
		virtual void Finalize();

		virtual void Tick();

		virtual void Clear();
		virtual void Draw();


	private:
		HRESULT CreateDescriptorHeaps();
		HRESULT CreateRenderTarget();
		HRESULT CreateDepthStencil();
		HRESULT CreateGraphicsResources();
		HRESULT CreateSamplerBuffer();
		HRESULT CreateConstantBuffer(const Buffer& buffer);
		HRESULT CreateIndexBuffer(const Buffer& buffer);
		HRESULT CreateVertexBuffer(const Buffer& buffer);
		HRESULT CreateTextureBuffer(const Image& image);

	private:
		static const uint32_t		kFrameCount = 2;
		ID3D12Device*				m_pDevice = nullptr;
		D3D12_VIEWPORT				m_ViewPort;
		D3D12_RECT					m_ScissorRect;
		IDXGISwapChain3*			m_pSwapChain = nullptr;
		ID3D12Resource*				m_pRenderTargets[kFrameCount];	//rendering buffer
		ID3D12CommandAllocator*		m_pCommandAllocator = nullptr;
		ID3D12CommandQueue*			m_pCommandQueue = nullptr;
		ID3D12RootSignature*		m_pRootSignature = nullptr;
		ID3D12DescriptorHeap*		m_pRtvHeap = nullptr;			//descriptos of GPU objects
		ID3D12DescriptorHeap*		m_pDsvHeap = nullptr;
		ID3D12DescriptorHeap*		m_pCbvSrvUavHeap = nullptr;
		ID3D12DescriptorHeap*		m_pSamplerHeap = nullptr;
		ID3D12PipelineState*		m_pPipelineState = nullptr;		// an object maintains the state of all currently set shaders
																	// and certain fixed function state objects
																	// such as the input assembler, tesselator, rasterizer and output manager
		ID3D12GraphicsCommandList*	m_pCommandList = nullptr;

		uint32_t					m_nRtvDescriptorSize;
		uint32_t					m_nCbvSrvDescriptorSize;
		ID3D12Resource*				m_pVertexBuffer = nullptr;
		D3D12_VERTEX_BUFFER_VIEW	m_VertexBufferView;

		//Synchronization objects
		uint32_t					m_nFrameIndex;
		HANDLE						m_hFenceEvent;
		ID3D12Fence*				m_pFence = nullptr;
		uint32_t					m_nFenceValue;
	
	};
}