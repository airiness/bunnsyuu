#include <objbase.h>
#include <tchar.h>
#include "D3D11GraphicsManager.h"
#include "WindowsApplication.h"
#include "SceneManger.h"
#include "AssetLoader.h"

namespace BuNnsyuu
{
	const char* simpleVSFile = "Shader/hlsl/SimpleVertexShader.cso";
	const char* simplePSFile = "Shader/hlsl/SimplePixelShader.cso";

	//D3D11GraphicsManager::VertexPosColor vertices[8] =
	//{
	//	{Vector3f{-1.0f, -1.0f, -1.0f},Vector3f{0.0f, 0.0f, 0.0f}},//,Vector3f{0.0f, 0.0f, 0.0f}},
	//	{Vector3f{-1.0f,  1.0f, -1.0f},Vector3f{0.0f, 1.0f, 0.0f}},//,Vector3f{0.0f, 1.0f, 0.0f}},
	//	{Vector3f{ 1.0f,  1.0f, -1.0f},Vector3f{1.0f, 1.0f, 0.0f}},//,Vector3f{1.0f, 1.0f, 0.0f}},
	//	{Vector3f{ 1.0f, -1.0f, -1.0f},Vector3f{1.0f, 0.0f, 0.0f}},//,Vector3f{1.0f, 0.0f, 0.0f}},
	//	{Vector3f{-1.0f, -1.0f,  1.0f},Vector3f{0.0f, 0.0f, 1.0f}},//,Vector3f{0.0f, 0.0f, 1.0f}},
	//	{Vector3f{-1.0f,  1.0f,  1.0f},Vector3f{0.0f, 1.0f, 1.0f}},//,Vector3f{0.0f, 1.0f, 1.0f}},
	//	{Vector3f{ 1.0f,  1.0f,  1.0f},Vector3f{1.0f, 1.0f, 1.0f}},//,Vector3f{1.0f, 1.0f, 1.0f}},
	//	{Vector3f{ 1.0f, -1.0f,  1.0f},Vector3f{1.0f, 0.0f, 1.0f}},//,Vector3f{1.0f, 0.0f, 1.0f}},
	//};

	//uint16_t indicies[36] =
	//{
	//	0, 1, 2, 0, 2, 3,
	//	4, 6, 5, 4, 7, 6,
	//	4, 5, 1, 4, 1, 0,
	//	3, 2, 6, 3, 6, 7,
	//	1, 5, 6, 1, 6, 2,
	//	4, 0, 3, 4, 3, 7
	//};

	template<typename T>
	inline void SafeRelease(T& ptr)
	{
		if (ptr != nullptr)
		{
			ptr->Release();
			ptr = nullptr;
		}
	}
}

int BuNnsyuu::D3D11GraphicsManager::Initialize()
{
	int result = 0;
	m_ClientHandle = reinterpret_cast<WindowsApplication*>(g_pApp)->GetMainWindow();
	GetClientRect(m_ClientHandle, &m_ClientRect);

	CreateGraphicsResources();

	return result;
}

void BuNnsyuu::D3D11GraphicsManager::Finalize()
{
	SafeRelease(m_pConstantBuffers[CB_Application]);
	SafeRelease(m_pConstantBuffers[CB_Frame]);
	SafeRelease(m_pConstantBuffers[CB_Object]);
	SafeRelease(m_pIndexBuffer);
	SafeRelease(m_pVertexBuffer);
	SafeRelease(m_pPixelShader);
	SafeRelease(m_pVertexShader);
	SafeRelease(m_pInputLayout);

	SafeRelease(m_pDepthStencilView);
	SafeRelease(m_pRenderTargetView);
	SafeRelease(m_pDepthStencilBuffer);
	SafeRelease(m_pDepthStencilState);
	SafeRelease(m_pRasterizerState);
	SafeRelease(m_pSwapChain);
	SafeRelease(m_pDeviceContext);
	SafeRelease(m_pDevice);

}

void BuNnsyuu::D3D11GraphicsManager::Tick()
{
	static float pos = 0.0f;
	pos += 0.001f;
	Vector3f eyePosition{ 0, 0, -10 };
	Vector3f focusPoint{ 0,0,0 };
	Vector3f upDirection{ 0,1,0 };
	BuildViewLHMatrix(m_ViewMatrix, eyePosition, focusPoint, upDirection);
	m_pDeviceContext->UpdateSubresource(m_pConstantBuffers[CB_Frame], 0, nullptr, &m_ViewMatrix, 0, 0);

	static float angle = 0.0f;
	angle += 0.001f;
	Vector3f rotationAxis{ 0.0f,1.0f,1.0f };
	MatrixRotationAxis(m_WorldMatrix, rotationAxis, angle);
	m_pDeviceContext->UpdateSubresource(m_pConstantBuffers[CB_Object], 0, nullptr, &m_WorldMatrix, 0, 0);

	Draw();
}

void BuNnsyuu::D3D11GraphicsManager::Clear()
{
	float clearColor[4] = { 0.392156899f, 0.584313750f, 0.929411829f, 1.000000000f };
	m_pDeviceContext->ClearRenderTargetView(m_pRenderTargetView, clearColor);
	m_pDeviceContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH | D3D10_CLEAR_STENCIL, 1.0f, 0);
}

void BuNnsyuu::D3D11GraphicsManager::Draw()
{
	assert(m_pDevice);
	assert(m_pDeviceContext);

	Clear();

	const uint32_t vertexStride = sizeof(VertexPosColor);
	const uint32_t offset = 0;

	m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &vertexStride, &offset);
	m_pDeviceContext->IASetInputLayout(m_pInputLayout);
	m_pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_pDeviceContext->VSSetShader(m_pVertexShader, nullptr, 0);
	m_pDeviceContext->VSSetConstantBuffers(0, 3, m_pConstantBuffers);
	m_pDeviceContext->RSSetState(m_pRasterizerState);
	m_pDeviceContext->RSSetViewports(1, &m_Viewport);
	m_pDeviceContext->PSSetShader(m_pPixelShader, nullptr, 0);
	m_pDeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);
	m_pDeviceContext->OMSetDepthStencilState(m_pDepthStencilState, 1);
	m_pDeviceContext->DrawIndexed(_countof(indicies), 0, 0);

	m_pSwapChain->Present(0, 0);

}

HRESULT BuNnsyuu::D3D11GraphicsManager::InitializeBuffers()
{
	assert(m_pDevice);
	HRESULT hr;

	auto& scene = g_pSceneManager->GetSceneForRendering();
	auto pGeometryNode = scene.GetFirstGeometryNode();
	while (pGeometryNode)
	{
		auto pGeometry = scene.GetGeometry(pGeometryNode->GetSceneObjectRef());
		assert(pGeometry);
		auto pMesh = pGeometry->GetMesh().lock();
		if (!pMesh) return hr;

		auto vertexPropertiesCount = pMesh->GetVertexPropertiesCount();
		auto vertexCount = pMesh->GetVertexCount();


	}


	//vertex buffer
	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(D3D11_BUFFER_DESC));

	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.ByteWidth = sizeof(VertexPosColor) * vertexcount;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(D3D11_SUBRESOURCE_DATA));

	resourceData.pSysMem = vertices;
	if (FAILED(hr = m_pDevice->CreateBuffer(&vertexBufferDesc, &resourceData, &m_pVertexBuffer)))
	{
		return hr;
	}

	//index buffer
	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(D3D11_BUFFER_DESC));

	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.ByteWidth = sizeof(uint16_t) * _countof(indicies);
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;

	resourceData.pSysMem = indicies;

	if (FAILED(hr = m_pDevice->CreateBuffer(&indexBufferDesc, &resourceData, &m_pIndexBuffer)))
	{
		return hr;
	}

	//constant buffer
	D3D11_BUFFER_DESC constantBufferDesc;
	ZeroMemory(&constantBufferDesc, sizeof(D3D11_BUFFER_DESC));

	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.ByteWidth = sizeof(Matrix4X4f);
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;

	if (FAILED(hr = m_pDevice->CreateBuffer(&constantBufferDesc, nullptr, &m_pConstantBuffers[CB_Application])))
	{
		return hr;
	}
	if (FAILED(hr = m_pDevice->CreateBuffer(&constantBufferDesc, nullptr, &m_pConstantBuffers[CB_Frame])))
	{
		return hr;
	}
	if (FAILED(hr = m_pDevice->CreateBuffer(&constantBufferDesc, nullptr, &m_pConstantBuffers[CB_Object])))
	{
		return hr;
	}

	return hr;
}

HRESULT BuNnsyuu::D3D11GraphicsManager::InitializeShaders()
{
	assert(m_pDevice);
	HRESULT hr;
	Buffer vertexShader = g_pAssetLoader->SyncOpenAndReadBinary(simpleVSFile);
	Buffer pixelShader = g_pAssetLoader->SyncOpenAndReadBinary(simplePSFile);

	//vertex shader
	if (FAILED(hr = m_pDevice->CreateVertexShader(vertexShader.GetData(), vertexShader.GetDataSize(), nullptr, &m_pVertexShader)))
	{
		return hr;
	}

	D3D11_INPUT_ELEMENT_DESC vertexLayoutDesc[] =
	{
		{"POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,0,offsetof(VertexPosColor,Position),D3D11_INPUT_PER_VERTEX_DATA,0},
		{"COLOR",0,DXGI_FORMAT_R32G32B32_FLOAT,0,offsetof(VertexPosColor,Color),D3D11_INPUT_PER_VERTEX_DATA,0},
		//{"NORMAL",0,DXGI_FORMAT_R32G32B32_FLOAT,0,offsetof(VertexPosColor,Normal),D3D11_INPUT_PER_VERTEX_DATA,0}
	};

	if (FAILED(hr = m_pDevice->CreateInputLayout(vertexLayoutDesc, _countof(vertexLayoutDesc), vertexShader.GetData(), vertexShader.GetDataSize(), &m_pInputLayout)))
	{
		return hr;
	}

	//pixel shader
	if (FAILED(hr = m_pDevice->CreatePixelShader(pixelShader.GetData(), pixelShader.GetDataSize(), nullptr, &m_pPixelShader)))
	{
		return hr;
	}

	//projection matrix
	uint32_t clientwidth = m_ClientRect.right - m_ClientRect.left;
	uint32_t clientheight = m_ClientRect.bottom - m_ClientRect.top;
	float aspect = static_cast<float>(clientwidth) / static_cast<float>(clientheight);

	BuildPerspectiveFovLHMatrix(m_ProjectionMatrix, 45.0f, aspect, 0.1f, 100.0f);
	m_pDeviceContext->UpdateSubresource(m_pConstantBuffers[CB_Application], 0, nullptr, &m_ProjectionMatrix, 0, 0);
	return hr;
}

HRESULT BuNnsyuu::D3D11GraphicsManager::CreateGraphicsResources()
{
	HRESULT hr;
	assert(m_ClientHandle != 0);

	uint32_t clientwidth = m_ClientRect.right - m_ClientRect.left;
	uint32_t clientheight = m_ClientRect.bottom - m_ClientRect.top;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = clientwidth;
	swapChainDesc.BufferDesc.Height = clientheight;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = m_ClientHandle;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Windowed = TRUE;

	UINT createDeviceFlags = 0;
#if _DEBUG
	createDeviceFlags = D3D11_CREATE_DEVICE_DEBUG;
#endif // _DEBUG
	D3D_FEATURE_LEVEL featurelevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
	};
	D3D_FEATURE_LEVEL featurelevel;

	if (FAILED(hr = D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		createDeviceFlags,
		featurelevels,
		_countof(featurelevels),
		D3D11_SDK_VERSION,
		&swapChainDesc,
		&m_pSwapChain,
		&m_pDevice,
		&featurelevel,
		&m_pDeviceContext)))
	{
		return hr;
	}

	//render target view
	ID3D11Texture2D* backBuffer;
	if (FAILED(hr = m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer)))
	{
		return hr;
	}

	if (FAILED(hr = m_pDevice->CreateRenderTargetView(backBuffer, nullptr, &m_pRenderTargetView)))
	{
		return hr;
	}

	SafeRelease(backBuffer);

	//depth buffer
	D3D11_TEXTURE2D_DESC depthStencilBufferDesc;
	ZeroMemory(&depthStencilBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
	depthStencilBufferDesc.ArraySize = 1;
	depthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilBufferDesc.CPUAccessFlags = 0;
	depthStencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilBufferDesc.Width = clientwidth;
	depthStencilBufferDesc.Height = clientheight;
	depthStencilBufferDesc.MipLevels = 1;
	depthStencilBufferDesc.SampleDesc.Count = 1;
	depthStencilBufferDesc.SampleDesc.Quality = 0;
	depthStencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;

	if (FAILED(hr = m_pDevice->CreateTexture2D(&depthStencilBufferDesc, nullptr, &m_pDepthStencilBuffer)))
	{
		return hr;
	}

	if (FAILED(hr = m_pDevice->CreateDepthStencilView(m_pDepthStencilBuffer, nullptr, &m_pDepthStencilView)))
	{
		return hr;
	}

	//setup depth/stencil state
	D3D11_DEPTH_STENCIL_DESC depthStencilStateDesc;
	ZeroMemory(&depthStencilStateDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	depthStencilStateDesc.DepthEnable = TRUE;
	depthStencilStateDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilStateDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilStateDesc.StencilEnable = FALSE;

	if (FAILED(hr = m_pDevice->CreateDepthStencilState(&depthStencilStateDesc, &m_pDepthStencilState)))
	{
		return hr;
	}

	D3D11_RASTERIZER_DESC raseterizerDesc;
	ZeroMemory(&raseterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	raseterizerDesc.AntialiasedLineEnable = FALSE;
	raseterizerDesc.CullMode = D3D11_CULL_BACK;
	raseterizerDesc.DepthBias = 0;
	raseterizerDesc.DepthBiasClamp = 0.0f;
	raseterizerDesc.DepthClipEnable = TRUE;
	raseterizerDesc.FillMode = D3D11_FILL_SOLID;
	raseterizerDesc.FrontCounterClockwise = FALSE;	//clockwise triangle
	raseterizerDesc.MultisampleEnable = FALSE;
	raseterizerDesc.ScissorEnable = FALSE;
	raseterizerDesc.SlopeScaledDepthBias = 0.0f;

	if (FAILED(hr = m_pDevice->CreateRasterizerState(&raseterizerDesc, &m_pRasterizerState)))
	{
		return hr;
	}

	//viewport to occupy the entire client area
	m_Viewport.Width = static_cast<float>(clientwidth);
	m_Viewport.Height = static_cast<float>(clientheight);
	m_Viewport.TopLeftX = 0.0f;
	m_Viewport.TopLeftY = 0.0f;
	m_Viewport.MinDepth = 0.0f;
	m_Viewport.MaxDepth = 1.0f;

	InitializeBuffers();
	InitializeShaders();

	return hr;
}
