#pragma once
#include <d3d11.h>
#include <d3dcompiler.h>
#include "GraphicsManager.h"
#include "PIMath.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

namespace BuNnsyuu
{
	class D3D11GraphicsManager : public GraphicsManager
	{
	public:
		struct VertexPosColor
		{
			Vector3f Position;
			Vector3f Color;
			//Vector3f Normal;
		};
	public:
		virtual int Initialize();
		virtual void Finalize();

		virtual void Tick();

		virtual void Clear();
		virtual void Draw();

	private:

		HRESULT InitializeBuffers();
		HRESULT InitializeShaders();
	 	HRESULT CreateGraphicsResources();
	private:
		enum ConstantBuffer
		{
			CB_Application,
			CB_Frame,
			CB_Object,
			NumConstantBuffers
		};

	private:
		ID3D11Device* m_pDevice = nullptr;
		ID3D11DeviceContext* m_pDeviceContext = nullptr;
		IDXGISwapChain* m_pSwapChain = nullptr;
		ID3D11RenderTargetView* m_pRenderTargetView = nullptr;
		ID3D11DepthStencilView* m_pDepthStencilView = nullptr;
		ID3D11Texture2D* m_pDepthStencilBuffer = nullptr;
		ID3D11DepthStencilState* m_pDepthStencilState = nullptr;
		ID3D11RasterizerState* m_pRasterizerState = nullptr;

		D3D11_VIEWPORT m_Viewport = {0};

		ID3D11InputLayout* m_pInputLayout = nullptr;
		ID3D11Buffer* m_pVertexBuffer = nullptr;
		ID3D11Buffer* m_pIndexBuffer = nullptr;

		ID3D11Buffer* m_pConstantBuffers[NumConstantBuffers];

		//shader data
		ID3D11VertexShader* m_pVertexShader = nullptr;
		ID3D11PixelShader* m_pPixelShader = nullptr;

		Matrix4X4f m_WorldMatrix;
		Matrix4X4f m_ViewMatrix;		//view matrix of camera
		Matrix4X4f m_ProjectionMatrix;	//projection matrix of camera, will transform objects from view space into clip space 
		
		HWND m_ClientHandle;
		RECT m_ClientRect;
	};
}
