#include <objbase.h>
#if defined(_DEBUG)
#include <iostream>
#endif // defined(_DEBUG)
#include <tchar.h>
#include "D3D12GraphicsManager.h"
#include "WindowsApplication.h"

using namespace BuNnsyuu;

namespace BuNnsyuu
{
	extern IApplication* g_pApp;

	template<typename T>
	inline void SafeRelease(T& ptr)
	{
		if (ptr != nullptr)
		{
			ptr->Release();
			ptr = nullptr;
		}
	}

	static void GetHardwareAdapter(IDXGIFactory4* pFactory, IDXGIAdapter1** ppAdapter)
	{
		IDXGIAdapter1* pAdapter = nullptr;
		*ppAdapter = nullptr;

		//find a bigger gpu
		for (UINT adapterIndex = 0; DXGI_ERROR_NOT_FOUND != pFactory->EnumAdapters1(adapterIndex, &pAdapter); adapterIndex++)
		{
			DXGI_ADAPTER_DESC1 desc;
			pAdapter->GetDesc1(&desc);

			if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
			{
				//Don't select basic render driver adapter
				continue;
			}

			//check directx12 support
			if (SUCCEEDED(D3D12CreateDevice(pAdapter, D3D_FEATURE_LEVEL_11_0, __uuidof(ID3D12Device), nullptr)))
			{
				break;
			}
		}

		*ppAdapter = pAdapter;
	}
}

HRESULT BuNnsyuu::D3D12GraphicsManager::CreateDescriptorHeaps()
{
	HRESULT hr;

	//describe and create a render target view (RTV) heap

	//describe and create a render target view descriptor heap
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
	rtvHeapDesc.NumDescriptors = kFrameCount;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	if (FAILED(hr = m_pDevice->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_pRtvHeap))))
	{
		return hr;
	}

	m_nRtvDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	//describe and create a depth stencil view (DSV) descriptor heap
	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
	dsvHeapDesc.NumDescriptors = 1;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	if (FAILED(hr=m_pDevice->CreateDescriptorHeap(&dsvHeapDesc,IID_PPV_ARGS(&m_pDsvHeap))))
	{
		return hr;
	}

	//Describe and create a Shader Resource View (SRV) and
	//Constant Buffer View (CBV) and
	//Unordered Access View (UAV) descriptor heap
	D3D12_DESCRIPTOR_HEAP_DESC cbvSrvUavHeapDesc = {};
	cbvSrvUavHeapDesc.NumDescriptors = kFrameCount	//FrameCount Cbvs
		+ 100;		//+ 100 for the SRV(Terxture)
	cbvSrvUavHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbvSrvUavHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	if (FAILED(hr = m_pDevice->CreateDescriptorHeap(&cbvSrvUavHeapDesc, IID_PPV_ARGS(&m_pCbvSrvUavHeap))))
	{
		return hr;
	}

	m_nCbvSrvDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	//Describe and create a sampler descriptor heap
	D3D12_DESCRIPTOR_HEAP_DESC samplerHeapDesc = {};
	samplerHeapDesc.NumDescriptors = 2048;
	samplerHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
	samplerHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	if (FAILED(hr = m_pDevice->CreateDescriptorHeap(&samplerHeapDesc, IID_PPV_ARGS(&m_pSamplerHeap))))
	{
		return hr;
	}

	if (FAILED(hr = m_pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_pCommandAllocator))))
	{
		return hr;
	}

	return hr;
}

HRESULT BuNnsyuu::D3D12GraphicsManager::CreateRenderTarget()
{
	HRESULT hr;

	D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = m_pRtvHeap->GetCPUDescriptorHandleForHeapStart();

	//create a rtv for every frame
	for (uint32_t i = 0; i < kFrameCount; i++)
	{
		if (FAILED(hr = m_pSwapChain->GetBuffer(i,IID_PPV_ARGS(&m_pRenderTargets[i]))))
		{
			break;
		}
		m_pDevice->CreateRenderTargetView(m_pRenderTargets[i], nullptr, rtvHandle);
		rtvHandle.ptr += m_nRtvDescriptorSize;
	}

	return hr;
}

HRESULT BuNnsyuu::D3D12GraphicsManager::CreateGraphicsResources()
{
	HRESULT hr;
#if defined(_DEBUG)
	//enable d3d12 debug layer
	{
		ID3D12Debug* pDebugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&pDebugController))))
		{
			pDebugController->EnableDebugLayer();
		}
		SafeRelease(pDebugController);
	}
#endif // defined(_DEBUG)

	IDXGIFactory4* pFactory;
	if (FAILED(hr=CreateDXGIFactory1(IID_PPV_ARGS(&pFactory))))
	{
		return hr;
	}

	IDXGIAdapter1* pHardwareAdapter;
	GetHardwareAdapter(pFactory, &pHardwareAdapter);

	if (FAILED(D3D12CreateDevice(pHardwareAdapter,D3D_FEATURE_LEVEL_11_0,IID_PPV_ARGS(&m_pDevice))))
	{
		IDXGIAdapter* pWarpAdapter;
		if (FAILED(hr=pFactory->EnumWarpAdapter(IID_PPV_ARGS(&pWarpAdapter))))
		{
			SafeRelease(pFactory);
			return hr;
		}

		if (FAILED(hr=D3D12CreateDevice(pWarpAdapter,D3D_FEATURE_LEVEL_11_0,IID_PPV_ARGS(&m_pDevice))))
		{
			SafeRelease(pFactory);
			return hr;
		}
	}

	HWND hWnd = reinterpret_cast<WindowsApplication*>(g_pApp)->GetMainWindow();

	//describe and create the command queue
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	if (FAILED(hr=m_pDevice->CreateCommandQueue(&queueDesc,IID_PPV_ARGS(&m_pCommandQueue))))
	{
		SafeRelease(pFactory);
		return hr;
	}

	//create a struct to hold information about the swap chain
	DXGI_SWAP_CHAIN_DESC1 scd;

	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC1));

	RECT rc = {};
	//fill the swap chain description struct
	GetClientRect(hWnd, &rc);

	scd.Width = rc.right - rc.left;
	scd.Height = rc.bottom - rc.top;
	scd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;				//32bit color
	scd.Stereo = FALSE;
	scd.SampleDesc.Count = 1;								//multi-samples
	scd.SampleDesc.Quality = 0;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.BufferCount = kFrameCount;
	scd.Scaling = DXGI_SCALING_STRETCH;
	scd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;			//only support on win10 ///DXGI_SWAP_EFFECT_DISCARD
	scd.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;		//allow fullscreen transition

	IDXGISwapChain1* pSwapChain;
	if (FAILED(hr = pFactory->CreateSwapChainForHwnd(
		m_pCommandQueue,
		hWnd,
		&scd,
		nullptr,
		nullptr,
		&pSwapChain)))
	{
		SafeRelease(pFactory);
		return hr;
	}

	m_pSwapChain = reinterpret_cast<IDXGISwapChain3*>(pSwapChain);

	m_nFrameIndex = m_pSwapChain->GetCurrentBackBufferIndex();

	if (FAILED(hr = CreateDescriptorHeaps()))
	{
		return hr;
	}
	if (FAILED(hr = CreateRenderTarget()))
	{
		return hr;
	}

	return hr;
}

int BuNnsyuu::D3D12GraphicsManager::Initialize()
{
	int result = 0;
	result = static_cast<int>(CreateGraphicsResources());
	return result;
}

void BuNnsyuu::D3D12GraphicsManager::Finalize()
{
	SafeRelease(m_pFence);
	SafeRelease(m_pVertexBuffer);
	SafeRelease(m_pCommandList);
	SafeRelease(m_pPipelineState);
	SafeRelease(m_pRtvHeap);
	SafeRelease(m_pRootSignature);
	SafeRelease(m_pCommandQueue);
	SafeRelease(m_pCommandAllocator);
	for (uint32_t i = 0; i < kFrameCount; i++)
	{
		SafeRelease(m_pRenderTargets[i]);
	}
	SafeRelease(m_pSwapChain);
	SafeRelease(m_pDevice);
}

void BuNnsyuu::D3D12GraphicsManager::Tick()
{
#if defined(_DEBUG)
	static auto i = 0;
	std::wcout << i++ << std::endl;
#endif // defined(_DEBUG)


}

void BuNnsyuu::D3D12GraphicsManager::Clear()
{
}

void BuNnsyuu::D3D12GraphicsManager::Draw()
{
}


