struct PixelShaderInput
{
    float4 color : COLOR;
    float4 position : POSITION;
    //float4 normal : NORMAL;
};

float4 SimplePixelShader(PixelShaderInput IN) : SV_TARGET
{
    float4 result = float4(IN.color * cos(IN.position));
    return result;
}